

.. index::
   pair: Livre ; Anarchisme, vent debout !
   pair: Auteur ; Philippe Pelletier



.. _vent_debout:

=======================================================================================================
Anarchisme, vent debout ! idées reçues sur le mouvement libertaire 3e édition de Philippe Pelletier
=======================================================================================================

.. seealso::

   - http://www.lecavalierbleu.com/livre/anarchisme-vent-debout/
   - :ref:`philippe_pelletier`
   

.. contents::
   :depth: 3


.. figure:: anarchisme_3ed.jpg
   :align: center
   

Quatre de couverture
======================


L'anarchisme est la conception politique, philosophique et sociale probablement 
la plus méconnue. 
Il est vrai qu'il ne se laisse pas facilement appréhender. Au-delà de quelques 
slogans comme « ni dieu, ni maître », il suscite de nombreuses idées reçues, 
souvent contradictoires. Les anarchistes seraient ainsi violents et/ou babas cool, 
marginaux et/ou syndicalistes, ils prôneraient un mode de vie spécifique et/ou 
s'agiteraient dans la révolution... 
De la fin du XIX e à ce début de XXI e siècle, l'anarchisme a alterné entre 
périodes d'avancées et de recul, mais jamais n'a disparu car « on peut détruire 
une organisation mais pas une idée, une idée toute simple : si tu veux être 
libre, sois le, mais avant tout apprend à l'être. » 

(Felix Carrasquer).

