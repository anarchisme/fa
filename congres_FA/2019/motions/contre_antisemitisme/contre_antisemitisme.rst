
.. index::
   pair: Antisémitisme; Motion juin 2019


.. _motion_antisemitisme_2019:

=====================================================================================
Motion sur l’antisémitisme, Fédération anarchiste 78ème Congrès (Amiens juin 2019)
=====================================================================================

.. seealso::

   - https://blogs.mediapart.fr/philippe-corcuff/blog/081119/antisemitisme-sur-radio-libertaire
   - https://www.federation-anarchiste.org/?g=FA_motions


Les chiffres des actes antisémites commis en 2018, et révélés par le ministère
de l’Intérieur sont effarants.

En un an, les actes recensés sont passés de 311 (en 2017) à 541, soit un bond de 74 %.

Pour autant, pour nous anarchistes, il ne s’agit pas d’une **résurgence de
l’antisémitisme**, parce que celui-ci a toujours été présent au cours
de l’histoire et se répand de manière exponentielle aujourd’hui, notamment avec
les réseaux sociaux.

L’antisémitisme, visant les Juifs, ou supposés tels, en tant que groupe religieux,
ethnique ou racial, n’est pas le seul apanage d’une droite extrême ou se voulant
"traditionnelle " ou "nationaliste".
Il réapparaît plus fort à chaque crispation identitaire.

De tous temps, de nombreux prétextes ont été utilisés pour justifier l’antisémitisme.
Mais l’antisémitisme, en tant qu’une des formes politiques du racisme, culmine
lors de la Conférence nazie de Wansee, pour définir les modalités administrative,
technique et économique, de la "solution finale de la question juive".

L’antisémitisme a également ciblé les Juifs par les purges staliniennes, comme
lors du **complot des blouses blanches**.

Après la Seconde guerre mondiale et l’extermination des Juifs, la
plupart des militant·es juifs et juives ayant disparu, s’en est donc
suivi un silence lourd de conséquence sur la Shoah, y compris dans les
rangs des militant·es anarchistes. Est-ce dû au fait que la Shoah nous
questionne profondément en tant qu’êtres humains ?

Toujours est-il que, non seulement l’extrême-droite, mais aussi des éléments issus
de l’extrême gauche ont commencé à développer des propos et des positions
révisionnistes voire négationnistes sur l’existence même
du massacre des Juifs,  alors qu’il est aujourd’hui acquis par les historien·nes
qu’entre 5,5 et 6,5 millions d’entre eux ont disparu durant ce génocide.

L’antisionisme est une autre question.

**Il est donc important de mobiliser toutes nos forces pour combattre tous propos
ou actes antisémites et de bien les dissocier de l’antisionisme**.

L’ignorance de ces faits alimente le négationnisme et le révisionnisme.

Les anarchistes ne traitent pas le nationalisme de l’Etat israélien autrement
que n’importe quel nationalisme.
L’Etat d’Israël est pour nous un Etat parmi tant d’autres, qui développe
aujourd’hui une politique raciste, colonialiste et sous pression religieuse.

Nous continuerons à soutenir les Anarchistes contre le mur en Israël, tout comme les
objecteurs·trices israélien·nes, de même nous soutenons la lutte de la population
palestinienne opprimée, et ce parce que directement au cœur des combats pour la
liberté de chacun·e.

Car nous avons bien conscience que la création de l’Etat israélien confirme la
thèse anarchiste que la création d’un Etat ne peut se faire que dans la violence.
Nous avons cependant également conscience que, se dire anti-impérialiste
ne suffit pas à se prémunir contre l’antisémitisme.

Aussi, en tant qu’anarchistes contre toutes les formes de discriminations et
d’oppressions, nous continuerons à lutter contre l’antisémitisme,
et à combattre toutes les formes de racisme, notamment à l’encontre des
migrant·es, des réfugié·es et des exilé·es, activement dans la rue,
mais aussi en renforçant nos moyens de diffusion (le Monde libertaire,
Radio libertaire, Editions du Monde libertaire, tracts, conférences, cycles
de formation, etc.) par des argumentaires et des recherches historiques.

Le racisme et l’antisémitisme sont des armes de ceux et celles qui
cherchent à diviser pour dominer. Nous les combattrons pied à pied.
