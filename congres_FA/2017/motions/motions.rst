

.. index::
   pair: Motions ; 2017



.. _motions_FA_2017:

========================================================================
Motions du 75ème congrés 
========================================================================


.. toctree::
   :maxdepth: 3
   
   salut_a_toi/salut_a_toi
   economie_et_sociale/economie_et_sociale
