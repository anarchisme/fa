
.. index::
   ! Philippe Pelletier


.. _philippe_pelletier:

=====================================
Philippe Pelletier
=====================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Philippe_Pelletier



.. contents::
   :depth: 3

Philippe Pelletier en 2015
============================

.. figure:: Philippe_Pelletier_octobre_2015.jpg
   :align: center
   
   
Livres
=======

2018
----

- :ref:`vent_debout`


2015
-----

- :ref:`camus_reclus`
